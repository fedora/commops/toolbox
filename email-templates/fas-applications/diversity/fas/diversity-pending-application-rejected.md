Diversity Team pending FAS group rejection email
================================================

This is a template to send as an email to a rejected applicant to the Fedora
Diversity Team FAS group. This is sent manually, in addition to the automatic
email sent by FAS when someone's application is rejected from a group.

The purpose of the email is to give a better explanation for why they were
rejected and offer opportunities to get involved.

* **Subject**: `$FAS_USERNAME: Your Diversity Team FAS application status`

```
Hello $FAS_USERNAME,

You are receiving this email because of your pending application in the
Fedora Diversity Team (diversity-team) group in the Fedora Account System (FAS).

Joining the FAS group is the last step of getting involved. You can see the
steps to become a sponsored member of the team on the Fedora wiki.

    https://fedoraproject.org/wiki/Diversity/Join

If you haven't already, send an introduction to the Diversity mailing list. In
the intro, tell us a bit about yourself and any background to why you want to
join the Diversity Team, or any skills you want to mention. This helps us help
you get started!

You can find our mailing list here.

    https://lists.fedoraproject.org/admin/lists/diversity@lists.fedoraproject.org/

It's not too late to get involved! See some of the tasks we're working on in our
Pagure issue tracker. Log into Pagure with your Fedora account. You can find our
Pagure here:

    https://pagure.io/fedora-diversity/issues

Something look interesting? Leave a comment on a ticket with a question or ask
if there's a way you can help. We always need help and we are happy to help you
get started on something. There's no such thing as silly or dumb questions!

If you're still interested in joining the Diversity Team, review the steps to
join (linked above). Take a look at some things we're working on and introduce
yourself. If you are available, join one of our meetings and say hello to the
team!

See when we meet in your time zone here:

    https://apps.fedoraproject.org/calendar/meeting/6397/

You're welcome to join our IRC channel too. The channel is #fedora-diversity on
irc.freenode.net. If you need help with IRC or are unfamiliar, check out this
guide to use Riot, a beginner-friendly IRC client.

    https://opensource.com/article/17/5/introducing-riot-IRC

If you have any questions, ask on our mailing list. If you prefer, you can reply
back to me directly, but you might get a slower answer. Thank you for your
interest in the Fedora Diversity Team. We hope to hear from you soon!

Sincerely,
- $NAME
```
