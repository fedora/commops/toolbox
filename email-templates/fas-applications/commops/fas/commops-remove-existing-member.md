CommOps inactive FAS group member removal email
===============================================

This template is for sending an email to a sponsored member in the Fedora
CommOps FAS group that is now inactive in CommOps. This is sent manually, in
addition to the automatic email they receive when removed from the group.

The purpose of the email is to give a better explanation for why they were
removed and to explain how to become an active member again.

* **Subject**: `$FAS_USERNAME: Your CommOps FAS group status`

```
Hello $FAS_USERNAME,

You are receiving this email because of your membership in the Fedora Community
Operations (commops) group in the Fedora Account System (FAS). Since we haven't
heard from you in a while or seen you in the team, you were removed from the
CommOps FAS group.


## I no longer want to contribute

If you no longer have interest in contributing to Fedora, then you do not need
to take any action. If you receive emails from CommOps, you should no longer
receive emails from Pagure automatically. If you receive mail on our mailing
list, you have to unsubscribe yourself.


## Wait, I still want to help!

Are you still interested in contributing to Fedora CommOps? If you are, it's not
too late to get involved. Since you were once a sponsored member, you can resume
contributing and skip some parts of our join process.

As a reminder, joining the CommOps FAS group is the last step of getting
involved. See the steps to become a sponsored member of the CommOps team on the
Fedora wiki.

    https://fedoraproject.org/wiki/CommOps/Join

If you've contributed in the last year and already introduced yourself on the
mailing list, you don't have to do this again. If it's been longer than a year,
please re-introduce yourself and tell us any background to why you want to join
CommOps or any skills you want to mention. This helps us help you get started!

You can find our mailing list here.

    https://lists.fedoraproject.org/admin/lists/commops@lists.fedoraproject.org/

We use a Pagure issue tracker to keep track of what we're working on. You can
log into Pagure with your Fedora account. Check out our Pagure repo here:

    https://pagure.io/fedora-commops/issues

Something look interesting? Leave a comment on a ticket with a question or ask
if there's a way you can help. We always need help and we are happy to help you
get started on something. There's no such thing as silly or dumb questions!

If you're still interested in joining the CommOps team, take a look at what
we're working on and re-introduce yourself. If you are available, join one of
our meetings and say hello to the team!

See when we meet in your time zone here:

    https://apps.fedoraproject.org/calendar/meeting/4409/

You're welcome to join our IRC channel too. The channel is #fedora-commops on
irc.freenode.net. If you need help with IRC or are unfamiliar, check out this
guide to use Riot, a beginner-friendly IRC client.

    https://opensource.com/article/17/5/introducing-riot-IRC


## Thank you

Regardless, thank you for your past contributions to the Community Operations
team. We appreciate the time and effort you spent with the team, even if you
didn't think it was much! Every contribution is helpful and we appreciate you
sharing a little bit of your time with us. I hope we will see you again someday,
somewhere else in the open source world.

If you have any questions, ask on our mailing list. If you prefer, you can reply
back to me directly, but you might get a slower answer. Thank you again!

Sincerely,
- $NAME
```
