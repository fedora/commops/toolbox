FAS Magazine application removal
================================

This text is a basic template to use for removed applications to the Magazine FAS group. This can be sent to accompany the automatic removal email sent by FAS, to help give a better explanation for why their application was removed.

* **Subject**: $FAS_USERNAME: Your Magazine FAS application status

- - - - -

```
Hello $FAS_USERNAME,

This email is being sent in regards to your request for membership in the Fedora Magazine Contributor Group in the Fedora Account System (FAS).

In order to become an approved member of the Magazine group, you will need to contribute to the Fedora Magazine first. To get started, you can send a self-introduction to our mailing list. In the intro, you can tell us a bit about who you are, any relevant background / skills you want to mention, and how we can help you get started.

You can find our mailing list here:

    https://lists.fedoraproject.org/admin/lists/magazine@lists.fedoraproject.org

If you want to contribute an article to the Fedora Magazine, we'd love to help! If you are wanting to write an article, you can see the workflow and steps to follow in order to get your article published. Check out this page for help getting started with writing:

    https://fedoramagazine.org/writing-an-article-for-the-fedora-magazine/

One of the important parts of this process will be putting together a pitch for the editorial team to review. This is mentioned in the above article. If you're not sure how to begin writing a pitch, you can see this page with some tips and how to format them.

    https://fedoramagazine.org/writing-a-new-pitch/

If you're still interested in contributing to the Fedora Magazine, drop an introduction to the mailing list and stop by our IRC channel on Freenode (#fedora-magazine) and say hello! Once you introduce yourself and have published an article or two, you can re-apply to the group in FAS.

Thanks for your interest! Hope to hear from you soon. Let me know if you have any questions about getting started!
```
