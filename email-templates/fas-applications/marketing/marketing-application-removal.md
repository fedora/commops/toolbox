FAS Marketing application removal
=================================

This text is a basic template to use for removed applications to the Marketing FAS group. This can be sent to accompany the automatic removal email sent by FAS, to help give a better explanation for why their application was removed.

* **Subject**: $FAS_USERNAME: Your Marketing FAS application status

- - - - -

```
Hello $FAS_USERNAME,

This email is being sent in regards to your request for membership in the Fedora Marketing Group in the Fedora Account System.

In order to become an approved member of the Marketing group, you need to join the Fedora Marketing mailing list to send a self-introduction. This self-introduction can include who you are, your background, any skills you want to mention, and how we can help you get started.

    https://fedoraproject.org/wiki/Introduce_yourself_to_the_marketing_group

You can see more on the Join wiki article for the entire process of joining the Marketing team.

    https://fedoraproject.org/wiki/Joining_the_Fedora_marketing_project

Send an introduction to the mailing list and stop by our IRC channel on Freenode (#fedora-mktg) and say hello! Once you do both of these things, you can re-apply to the group in FAS.

Thanks, hoping to hear from you soon! Let me know if you have any questions.
```
